
## Overview

Check client id is online or not

## Build

In emqttd project:

```
git submodule add https://524172394@bitbucket.org/524172394/emqttd_check_client_id_is_online.git plugins/emqttd_check_client_id_is_online

make
```

## Configure

```
[
  {emqttd_check_client_id_is_online, [
    {listener, 
        {emqttd_check_client_id_is_online, 18088, [
            {acceptors, 4},
            {max_clients, 512}]}}
  ]}
].
```

## Load Plugin

```
./bin/emqttd_ctl emqttd_check_client_id_is_online
```

## Login 

URL: http://host:18088/check_client_is_online?clientId=#{clientId}


