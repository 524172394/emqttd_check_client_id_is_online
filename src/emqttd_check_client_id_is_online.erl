%%%-----------------------------------------------------------------------------
%%% @Copyright (C) 2012-2015, Feng Lee <feng@emqtt.io>
%%%
%%% Permission is hereby granted, free of charge, to any person obtaining a copy
%%% of this software and associated documentation files (the "Software"), to deal
%%% in the Software without restriction, including without limitation the rights
%%% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%%% copies of the Software, and to permit persons to whom the Software is
%%% furnished to do so, subject to the following conditions:
%%%
%%% The above copyright notice and this permission notice shall be included in all
%%% copies or substantial portions of the Software.
%%%
%%% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%%% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%%% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%%% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%%% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%%% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
%%% SOFTWARE.
%%%-----------------------------------------------------------------------------
%%% @doc
%%% emqttd check clientId is online
%%%
%%% @end
%%%-----------------------------------------------------------------------------
-module(emqttd_check_client_id_is_online).

-author("Linh <linh@innov8tion.com>").

-export([handle_request/1]).

clients(["check_is_online", ClientId]) ->
    lager:debug("test find cliendId is online ?~n"),
    case emqttd_cm:lookup(list_to_binary(ClientId)) of
        undefined ->
            lager:info("Not Found,so is not online.~n"),
            R = "false";
        Client ->
            lager:info("find,so client is online"),
            R = "true"
    end,
    R.

handle_request(Req) ->
            ReqPath = Req:get(path),
            lager:info("req path:~s~n",[ReqPath]),

            QueryStringData = Req:parse_qs(),
            QueryClientId= proplists:get_value("clientId", QueryStringData, "Anonymous"),
            lager:info("check client id:~s~n",[QueryClientId]),
            Bodys = clients(["check_is_online",QueryClientId]),
            Req:respond({200, [{"Content-Type", "text/plain"}],
                    Bodys}).
