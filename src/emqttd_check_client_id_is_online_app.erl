-module(emqttd_check_client_id_is_online_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    {ok, Sup} = emqttd_check_client_id_is_online_sup:start_link(),
    {ok, Listener} = application:get_env(emqttd_check_client_id_is_online, listener),
    open_listener(Listener),
    {ok, Sup}.

stop(_State) ->
    {ok, {_Proto, Port, _Opts}} = application:get_env(emqttd_dashboard, listener),
    mochiweb:stop_http(Port).

%% open http port
open_listener({_Http, Port, Options}) ->
    MFArgs = {emqttd_check_client_id_is_online, handle_request, []},
	mochiweb:start_http(Port, Options, MFArgs).


